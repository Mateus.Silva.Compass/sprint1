# Sprint 1

## Dia 01: 12/06/2023

**Tarefas:** 
- Onboarding para explicar sobre a Bolsa na Uol e sobre a empresa.
- Kickoff para conhecer o time que farei parte. 

**Aprendendo sobre GitLab e ReadMe:**

<img width="160" height="100" src="https://download.logo.wine/logo/GitLab/GitLab-Logo.wine.png" />

<img width="200" height="50" src="https://th.bing.com/th/id/OIP.p0dki4GlHZ_hc-dY2EusBAHaBV?pid=ImgDet&rs=1" />

- O GitLab é um gerenciador de repositório de software baseado em git, com suporte a Wiki, gerenciamento de tarefas e CI/CD.
- O Readme é um arquivo de texto utilizado para descrever o seu projeto.


## Dia 02: 13/06/2023

**Tarefas:**  
- Assistir o Curso "Fundamentos do Scrum: Rápido e Prático".
- Utilização do Proj4me para aprendizado. 

**Sobre a Metodologia Ágil:**
- A metodologia Ágil surgiu e foi formalizada por volta da década de 1990, embora só começou a ser mais usada no século 21. Ela surge como uma alternativa à metodologia tradicional.
- A maior diferença entre a metodologia Ágil e a metodologia tradicional, é a entrega frequente de valor, que a metodologia ágil proporciona ao cliente durante o projeto, fazendo com que o cliente participe junto do processo de desenvolvimento do software, para que no final, o cliente saia satisfeito com o produto que foi entregue. 
- Não existe uma metodologia melhor ou pior. Cada uma é importante e depende de qual das duas é a melhor abordagem para determinado projeto. 
- Na metodologia ágil, mudanças são bem-vindas e absorvidas facilmente.

**Sobre Scrum:** 
- O Scrum é um framework que ajuda as pessoas, times e organizações a gerar valor por meio de soluções adaptativas para problemas complexos.
- No Scrum, o processo deve ser adaptável a mudanças técnicas e de negócio. 
- O processo de Scrum envolve as seguintes atividades: levantamento de requisitos, análise, projeto, evolução e entrega. 
- As tarefas de cada atividade são feitas dentro de um padrão de processo chamado “sprint”.
- Um sprint é uma unidade de trabalho com tempo pré-determinado. 
- Uma lista priorizada de requisitos, dita backlog, é mantida. 
- No backlog se têm uma lista de requisitos priorizados.
- Nessa lista de requisitos priorizados, estão as histórias de usuário. 
- Uma história de usuário é uma representação clara e informal que expressa a necessidade de um potencial usuário.
- Pequenas reuniões são feitas diariamente pela equipe com o Scrum Master. É as Daily Scrum.
- A Daily Scrum serve como uma inspeção do progresso em direção a meta da Sprint.
- No final de toda sprint é feito um feedback, na qual o cliente vai revisar e vai aprovar total ou parcialmente a entrega da sprint. O que não for aprovado, volta para o backlog. 
- Retrospectiva da sprint: Lições aprendidas. Registrados os erros e acertos da sprint, com o objetivo em aprender com erro e crescer junto com o time.
- Um exemplo de demonstrar o funcionamento do Scrum é pela imagem a seguir: 

<img width="460" height="300" src="https://www.tecnicon.com.br/upload/public/Blog/post-scrum.png" />

## Dia 03: 14/06/2023

**Tarefas:** 
- Assistir o curso: "Início Rápido em Teste e QA".
- Assistir a Palestra: Webinar "Caminhos de Sucesso pelo Programa de Bolsas".

**Sobre a Carreira em Testes de Software e QA:**
- Uma carreira boa é quando: Há vagas. Boa remuneração. Oportunidade de evolução. Área em expansão. Empregos do mundo inteiro. Qualidade de vida. 
- Formas de contratação: Como freelancer, CLT, PJ, Empreendedor etc. 
- É uma área bem diversa, que possui oportunidade para todos. Manter-se atualizado é importante.
- Por que o desenvolvedor não testa? Ele testa sim, mas existem defeitos que ele não encontrará.
- Todos podem testar.

## Dia 04: 15/06/2023

**Tarefas:**
- Continuação do Curso: "Início Rápido em Teste e QA". 

**Perfil Profissional de Teste e QA**

***Soft Skils***: 
- Habilidades pessoais: possuir boa resiliência, ser motivado, gostar de aprender.
- Habilidades interpessoais Saber ouvir, ter postura, falar bem, coleguismo, ter empatia com o próximo.
- Saber trabalhar em equipe: Realizar reuniões, tirar dúvidas, buscar ajudar o time. Colaborar. Fazer junto. Estar em constante aprendizado. 

<img width="400" height="150" src="https://s3.amazonaws.com/ibc-portal/wp-content/uploads/2016/05/22115051/o-que-e-conhecimento.jpg" />

***Hard Skils***:

- Possuir conhecimentos gerais sobre o sistema operacional, saber mexer no e-mail, office, videoconferência etc.
- Possuir conhecimento sobre o negócio. O que vai testar? Banco, seguradora, comércio, indústria. 
- Possuir conhecimento sobre a tecnologia. Saber ao menos alguma linguagem de programação, saber sobre telecomunicações, saber sobre a infraestrutura e banco de dados. 
- Possuir conhecimento sobre Teste e QA. Analisar quais os melhores testes para fazer, criar os testes que for montar. Executar os testes: saber comunicar os defeitos e acompanhar os defeitos. Saber técnicas de testes e sobre práticas, processos, estratégias e ferramentas de testes. 
<img width="400" height="150" src="https://1.bp.blogspot.com/-EgD4mWoOwtc/Xk8o5QJi8nI/AAAAAAAAoP8/-VI_j76SP7I0NUIxinh7WqpEOJViVhXXACLcBGAsYHQ/w1200-h630-p-k-no-nu/AAEAAQAAAAAAAAbmAAAAJDAwZTYzOWJkLTMxZWEtNGM0YS1iMjQxLTIwOTdkYjI1Yzk0YQ.jpg" />

## Dia 05: 16/06/2023

**Tarefas:** 
- Continuação do curso "Início Rápido em Teste e QA". 
- [Realização de atividade em grupo](https://compasso-my.sharepoint.com/:p:/r/personal/filipe_alves_pb_compasso_com_br/Documents/Arquivos%20de%20Chat%20do%20Microsoft%20Teams/Como%20gerar%20qualidade%20em%20um%20projeto%20-%20Atualizado.pptx?d=w8e8f843cebe540a1a8aa0e6c50a4df08&csf=1&web=1&e=98Hokq).

**História do Teste**
- O software e os defeitos sempre conviveram junto. 
- Alguns nomes importantes da história: Charles Babbage, Ada Lovelance (Primeira programadora), Holerite (Introduziu o Cartão Perfurado), Alan Turing (Criador da máquina de Turing), Grace Hooper (Fez o registro do primeiro defeito/bug), Gleen Ford (Regra de 10 de Myers). 
- Esses são alguns dos nomes, dentro vários, de grande contribuição para a programação e para os testes, na história.
- Testar é uma coisa séria, tem que saber os riscos que eles podem ocasionar para as pessoas. Tem que se jogar com tudo. É muito importante testar com consciência e com sabedoria. É um seguro para fazer com que o software rode bem e com o mínimo de erros possíveis.

**Os 7 fundamentos do teste:**
- O teste pode demonstrar a presença de defeitos, mas não pode provar que eles não existem.
- Teste Exaustivo não é possível. 
- Teste antecipado. 
- Agrupamento de defeitos. 
- Paradoxo do Pesticida. 
- Teste depende do contexto. 
- Ausência de erros é uma ilusão. 

***Teste x QA***
- Eles se complementam, mas o teste é focado em avaliar o produto. Ver se o software ou aplicativo está funcionando conforme o cliente gostaria. 
- O QA quer melhorar o produto. Identificar oportunidades de melhoria. É a qualidade do processo de desenvolvimento, o processo como um todo da empresa. Melhorando a qualidade, melhora o produto. 
- Teste trabalha no produto e QA trabalha no processo. 

<img width="300" height="100" src="https://th.bing.com/th/id/OIP.PlKePfJmW5mPSv6PtMZV1QHaEK?pid=ImgDet&rs=1" />


***Erro x ocorrência x defeito x falha***

- Pessoas cometem erros (enganos), que produzem defeitos (bugs) no código. 
- Ocorrência é quando acha algo errado no trabalho de outro, e a gente dá uma chance para a pessoa parar pra pensar se aquilo está certo ou não. 
- Se um defeito no código for executado, o sistema falhará ao tentar fazer o que deveria, causando uma falha. 
- Nem todos os defeitos causam falhas (a falha só existe quando o defeito é executado).

Tipos de testes baseado na **IEC/ISO 25010**. Essa norma vai se subdivir em ***8 categorias***:

- Adequação Funcional: Se ele cumpre seu propósito, se ele respeita sua funcionalidade.
- Usabilidade: É a facilidade que o usuário vai ter em usar o programa. Quanto menos o usuário tiver de treinamento, o software vai ter uma melhor usabilidade.
- Compatibilidade: é saber se um software é compátivel com outros software's.
- Confiança: É saber que aquele software está sempre disponível para meu uso, é saber que ele vai sempre funcionar, mesmo que existam problemas. 
- Eficiência: É saber se o nosso software é rápido, precisa ter uma boa velocidade.
- Manutenibilidade: É a facilidade de dar manutenção a um software. 
- Portabilidade: É verificar se aquele software funciona em vários aparelhos, em vários locais diferentes.
- Segurança: É a segurança do software. É saber se seu uso é confiável. 

**Testes Manuais x Testes automatizados**

- Testes manuais: é navegar pelo programa, usar o aplicativo com as próprias mãos. Tem que conhecer bem o produto, conhecer bem o aplicativo, é um conhecimento sobre teste e da própria ferramenta. Teste manual está diminuindo o n° de vagas. 
- Testes automatizados: Um processo de construção e teste automatizado ocorre em uma base diária e detecta erros de integração de modo antecipado e rápido. Uma boa cobertura nos testes de regressão automatizados ajuda no desenvolvimento (e teste) de grandes sistemas integrados.
- O mercado procura por uma pessoa que tenha o foco na solução e não no problema. Tem que saber realizar o teste manual e o automatizado. Eles se complementam. Pessoa tem que saber ser multidisciplinar. Ser versátil. Saber realizar várias formas de testar.  
<img width="300" height="100" src="https://auditeste.com.br/wp-content/uploads/2020/08/testesautomatizados_testesmanuais-1-1000x481.png" />

**Testes tradicionais x Testes Ágeis**

- As empresas mudaram, o mercado mudou. As exigências do cliente subiram. A necessidade de realizar entregas frequentes virou natural. Você precisa fazer estratégias que permita realizar os testes que você precisa dentro de uma sprint (2-4 semanas).
- Hoje o testador é um membro ativo do projeto. Fazer com que os demais enxergarem como a qualidade é importante para o produto, que a qualidade é um esforço do time. 
- Melhorou o ambiente de trabalho, porque agora o testador é um membro importante do time. Melhorou a comunicação, reduziu atritos. 

<img width="300" height="150" src="https://www.clippingworld.com/wp-content/uploads/2020/04/Post-production-1024x576.jpg" />

## Dia 06: 19/06/2023

**Tarefas:**
- Fundamentos do Teste de Software.
- Assistir a palestra: Webinar "Inteligência Artificial Generativa". 

**Pirâmide de Teste**

<img width="250" height="120" src="https://files.speakerdeck.com/presentations/a539a6bbeb6d460481dc698034ee0c7d/slide_34.jpg" />

- Teste de unidade: Verificam o funcionamento da menor unidade de código testável da nossa aplicação. A unidade vai ser sempre definida como a menor parte testável do seu sistema.
- Teste de integração: Avalia a funcionalidade de vários módulos, quando integrados para formar uma única unidade. Seu objetivo é verificar se um conjunto de unidades se comporta da maneira correta. 
- Teste E2E: São os testes de ponta a ponta, que simulam um ambiente real. Verificam se aconteceu o que era esperado. Seu objetivo é imitar o comportamento do usuário final das nossas aplicações.
- Uma boa ***estrutura*** para os testes seria: Configurar os dados do teste; chame seu método em teste; e afirmar que os resultados esperados são retornados. 
- É importante que boa parte do código seja realizado por testes de unidade, já que eles rodam muito rápido e são mais simples (de fazer e manter). Já os testes de ponta a ponta devem possuir menos testes, pois são mais complexos e demorados. Os testes de integração existem para quando não podem ser cobertos por testes de ponta e ponta, e em de unidade. 

## Dia 07: 20/06/2023

**Tarefas:** 
- Assistir o Curso da AWS: "Cloud Essentials Learning Plan"

**Parte 1: Funções de Trabalho na Nuvem**

***Módulo 1: Funções da TI Clássica***

- Função on-premises Arquiteto: É o responsável por projetar arquiteturas de TI com base nos requisitos de negócio. 
- Função on-premises Administrador de sistemas: É o responsável pela instalação, suporte e manutenção de sistemas de computação e servidores. 
- Função on-premises Administrador de aplicações: É o responsável pelas aplicações empresariais. 
- Função on-premises Administrador de banco de dados: É responsável pela instalação e manutenção de banco de dados no ambiente de TI. É também responsável pelo treinamento de funcionários sobre o uso dos bancos de dados. 
- Função on-premises Administrador de rede: É o responsável pelo projeto, instalação, configuração e manutenção de LANs e WANs. 
- Função on-premises Administrador de armazenamento: É o responsável pelo gerenciamento dos sistemas de armazenamento no ambiente de TI.
- Função on-premises Administrador de segurança: É responsável pela instalação, configuração, gerenciamento, monitoramento e adoção de soluções de segurança. 

***Módulo 2: Funções na Nuvem***

- Função na nuvem Arquiteto enterprise de nuvem: É responsável por fornecer serviços de nuvem para a empresa. 
- Função na nuvem Gerente de Programa: É responsável por garantir que a nuvem seja gerenciada adequadamente. 
- Função na nuvem Gerente financeiro: É responsável pelo gerenciamento dos controles financeiros para a nuvem. 
- Função na nuvem Arquiteto da infraestrutura de nuvem: É responsável por criar as arquiteturas com base nas soluções de infraestrutura de nuvem. 
- Função na nuvem Engenheiro de operações em nuvem: É responsável por criar, monitorar e gerenciar a infraestrutura de nuvem e os serviços compartilhados. Alguns engenheiros são responsáveis especificamente pelo gerenciamento e suporte operacional aos serviços de nuvem. 
- Função na nuvem Arquiteto de segurança em nuvem: É responsável por especificar os requisitos de segurança. 
- Função na nuvem Engenheiro de operações de segurança: É responsável por gerenciar, monitorar e impor a segurança. 
- Função na nuvem Arquiteto de aplicações: É responsável por projetar aplicações otimizadas para a nuvem. 
- Função na nuvem Desenvolvedor de aplicações: É responsável pelo desenvolvimento de aplicações. Ele gerencia as alterações, as versões e implantações do código. 
- Função na nuvem Engenheiro de DevOps: É responsável pela criação e operação de fluxos de trabalho rápidos e dimensionáveis. 
<img width="250" height="100" src="https://th.bing.com/th/id/OIP.FhOuJo2z7FS16gNu_kjNMgAAAA?pid=ImgDet&rs=1" />

***Módulo 3: Funções na nuvem e infraestrutura como código***

- Por que usar infraestrutura como código? É uma prática em que a infraestrutura é fornecida e gerenciada usando técnicas de desenvolvimento de código e de software, como o versionamento e a integração e distribuição contínuas. 
- A infraestrutura como código é uma prática em que a infraestrutura é provisionada e gerenciada usando técnicas de desenvolvimento de código e de software, como o versionamento e a integração e distribuição contínuas. 

## Dia 08: 21/06/2023

**Tarefas:** 
- Continuação do Curso da AWS: "Cloud Essentials Learning Plan"

**Parte 2: Fundamentos do AWS Cloud Practitioner**

<img width="120" height="120" src="https://th.bing.com/th/id/OIP.rMRU7RpMwOeAzJ2O5iO3LQHaHU?pid=ImgDet&rs=1" />

- Fundamentos da nuvem da AWS: Modelo Cliente-Servidor (cliente faz uma solicitação e o servidor responde essa solicitação). 
- O que é computação em nuvem: É a entrega de recursos de TI sob demanda pela internet com uma definição de preço de pagamento pelo que usa.
- Benefícios da computação em nuvem: Trocar despesas iniciais por despesas variáveis; parar de adivinhar capacidade; aumentar velocidade e agilidade etc. 
- O Amazon Elastic Compute Cloud (Amazon EC2) fornece capacidade computacional segura e redimensionável na nuvem como instâncias do Amazon EC2. 
- Amazon Simple Notification Service (SNS) é um serviço de publicação/assinatura. 
- Amazon Simple Queue Service (SQS) é um serviço de enfileiramento de mensagens. 
- AWS Lambda é um serviço que permite a execução de códigos sem a necessidades de provisionar ou gerenciar servidores.
- Amazon ECS é um sistema de contêineres altamente dimensionável e de alto desempenho. 
- Amazon EKSé um serviço totalmente gerenciado que pode ser utilizado para executar Kubernetes na AWS. 
- AWS Fargate: é um mecanismo de computação sem servidor para contêineres. Funciona com o Amazon ECS e EKS. 
- AWS Elastic Beanstalk fornece definições de código e configuração. 
- AWS CloudFormation você pode considerar sua infraestrutura como código. 
- O amazon VPC permite que você provisione uma seção isolada da nuvem AWS.
- O AWS Direct Connect é um serviço que permite estabelecer uma conexão privada dedicada entre seu data center e uma VPC. 
- Sub-rede é uma seção de uma VPC na qual você pode agrupar recursos com base em necessidades operacionais ou de segurança. Podem ser públicas ou privadas.
- Lista de controle de acesso (ACL) de rede é um firewall virtual que controla o tráfego de entrada e saída do nível de sub-rede. 
- Amazon Route 53: é um serviço web de DNS. 
- Armazenamento de instâncias: fornece armazenamento temporário a nível de bloco para uma instância do Amazon EC2.
- Armazenamento de objetos: cada objeto consiste em dados, metadados e uma chave. 
- Amazon S3 é um serviço que fornece armazenamento a nível do objeto. 
- Armazenamento de arquivos: Vários clientes podem acessar dados armazenados em pastas de arquivos compartilhadas. 
- Banco de dados relacionados: os dados armazenados de forma que se relacionam a outras partes de dados. Usam linguagem de consulta estrutura (SQL) para armazenar e consultar dados. 
<img width="220" height="120" src="https://cl9.com.br/wp-content/uploads/2020/09/quais-sao-os-bancos-de-dados-mais-conhecidos.jpg" />

- Banco de dados não relacional: Utilizam estruturas diferentes de linhas e colunas para organizar tudo. É uma tabela onde você pode armazenar e consultar dados. 
- Amazon Relational Database Service (RDS) é um serviço que permite executar bancos de dados relacionados na nuvem AWS. 
- AWS Identifity and Acess Managemet (IAM) é o serviço que permite gerenciar o acesso aos serviços e recursos AWS com segurança. 
- Amazon CloudWatch: é um serviço web que permite monitorar e gerenciar várias métricas e configurar ações de alarme de acordo com os dados dessas métricas. 
- Existem outros vários serviços da Amazon, mas aqui relatei os que achei mais interessante e importante. 

## Dia 09: 22/06/2023

**Tarefas:** 
- Continuação do Curso da AWS: "Cloud Essentials Learning Plan"

**Introdução a Aquisição da Nuvem**

- Partes da adoção: Aquisição, Departamento Jurídico, Segurança, Administração, Finanças e Conformidade.

<img width="220" height="120" src="https://th.bing.com/th/id/OIP.cPmwlw74aCT-f6CnKDa_YgHaDp?pid=ImgDet&rs=1" />

- Para garantir a aquisição eficaz da nuvem, é necessário envolver todas as partes interessadas na organização, e trabalhar com as partes internas. 
- É importante entender o que está sendo comprado. Os fornecedores de serviços de nuvem auxiliam na compreensão das práticas de nuvem. 
- Aspectos principais da aquisição: Preço, segurança, administração, termos e condições. 
- A sustentabilidade se tornou um meio importante, por conta da necessidade de focar na eficiência, inovação e infraestrutura.

***Well-Architected Framework***

- Pilares da Well-Architected: Excelência operacional, segurança, confiabilidade, eficiência de performance, otimização de custos.
- Organização: Envolvimento da equipe de negócios e de desenvolvimento na definição das prioridades operacionais.
- Segurança: A proteção de dados envolve identificar e classificar os dados.
- Confiabilidade: é a capacidade de uma carga de trabalho executar uma função pretendida e consistente quando se esperado. 
- Pilar da otimização de custos: Capacidade de alcançar resultados de negócios no ponto de preço mais baixo. 
- Na nuvem, podemos parar de adivinhar as capacidades, testar os sistemas, facilitando a experimentação, permitindo a evolução das arquiteturas e aprimorando os dias de testes. 
- Permite que tomemos decisões conscientes, pensando nativamente na nuvem e compreendendo o possível impacto que realizamos. 

## Dia 10: 23/06/2023

**Tarefas**
- Apresentação do Challenge. 
- Revisão da Sprint. 

Sprint 1 concluída 🥳
