Referências

BALLERINI, R. Como usar Git e Github na prática! - desde o primeiro commit até o pull request! 2/2. Youtube, 2021. Disponível em: https://www.youtube.com/watch?v=UBAX-13g8OM. Acesso em: 12/06/2023.

CAMPOS, C.  A pirâmide de testes. 2019. Disponível em: https://medium.com/creditas-tech/a-pir%c3%a2mide-de-testes-a0faec465cc2. Acesso em: 19 jun. 2023.

CORREIA, J. (2023). Início Rápido em Teste e QA [Udemy]. Disponível em: https://www.udemy.com/course/inicio-rapido-em-teste-de-software-e-qa/.

FALBO, R.A, UFES - Notas de Aula – Engenharia de Software, 2014.

FARIAS, G. (2023). Fundamentos do Scrum Agile | Rápido e Prático [Udemy]. Disponível em: https://compassuol.udemy.com/course/fundamentos-scrum-agile/learn/lecture/24343530?learning_path_id=5439814#overview
